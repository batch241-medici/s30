// Count operator to count the total number of fruits on sale.

/*db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$onSale", fruitsOnSale: {$sum: 1}}},
	{$project: {_id:0}}
])*/

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$count: "fruitsOnSale"}
])

// Count operator to count the total number of fruits with stock more than 20.

/*db.fruits.aggregate([
	{$match: {stock: {$gte: 20}}},
	{$group: {_id: "$supplier_id",
	enoughStock: {$sum: 1}}},
	{$project: {_id:0}}
])*/

db.fruits.aggregate([
	{$match: {stock: {$gte: 20}}},
	{$count: "enoughStock"}
])

// Average operator to get the highest price of a fruit per supplier.

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id",
	avg_price: {$avg: "$price"}}},
])

// Max operator to get the highest price of a fruit per supplier.

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id",
	max_price: {$max: "$price"}}},
])

// Min operator to get the lowest price of a fruit per supplier.

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id",
	min_price: {$min: "$price"}}},
])